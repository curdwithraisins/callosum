import React from 'react';
import { Route, IndexRoute, IndexRedirect } from 'react-router';
import App from '../containers/App/';
import Main from '../containers/Main/';
import Page404 from '../components/Page404';

export default (
    <Route path="/" component={App}>
        <IndexRoute />
        <Route path="/leads/:leadId" component={Main} />
        <Route path="*" component={Page404} />
    </Route>
);
