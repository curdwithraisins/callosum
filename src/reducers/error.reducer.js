import actionsType from '../actions/types/error.types';

export default function (state = {
    err: null
}, action ){
    switch (action.type) {
        case actionsType.ERROR_REQUEST_NUMBER:
            return action.err;
    }
    return state;
}