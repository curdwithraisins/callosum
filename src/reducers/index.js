import { combineReducers } from 'redux';
import getReducer from './get.reducer';
import postReducer from './post.reducer';
import errorReducer from './error.reducer';

const rootReducer = combineReducers({
  getReducer,
  postReducer,
  errorReducer
});

export default rootReducer;
