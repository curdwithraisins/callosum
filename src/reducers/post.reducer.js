import actionsType from '../actions/types/post.types';

export default function (state = {
    lead: null,
    err: null
}, action){
    switch (action.type) {
        case actionsType.POST_SUCCESS:
            return Object.assign({}, state);
        case actionsType.POST_ERROR:
            return Object.assign({}, state);
    }
    return state;
}