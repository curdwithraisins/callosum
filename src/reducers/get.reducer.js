import actionsType from '../actions/types/get.types';

export default function (state = {
    lead: null,
    err: null
}, action ){
    switch (action.type) {
        case actionsType.GET_SUCCESS:
            return Object.assign({}, state.lead = action.data);
        case actionsType.GET_ERROR:
            return Object.assign({}, state.err = action.err);
    }
    return state;
}