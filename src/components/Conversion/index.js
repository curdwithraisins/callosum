import React, { Component } from 'react';

export default class Conversion extends Component {
    render() {
        const { conversion, availableCash, monthlyCash } = this.props;
        if (!conversion) return null;
        let rows = new Array(3);
        return (
            <div className="col-right">
                <section className="score">
                    <h2>CALLOSUM SCORE</h2>
                    <div className="text-frame gray-text">
                        <div className="img-left"><img src="assets/images/img-13.png" alt="img description" /></div>
                        <div className="text-right">
                            <strong className="title">Real and full information</strong>
                            <p>Explaination of the score on a human language, in order for a sales person to understand the meaning</p>
                        </div>
                    </div>
                </section>
                <section className="conversion">
                    <h2>conversion</h2>
                    <ul className="rating-list">
                        {
                            conversion.map((conv, i) => {
                                return <li key={i}>
                                <p>{conv.type} <a href="#" className="question"><img src="assets/images/img-14.png"
                                                                                      alt="img description"/></a></p>
                                <div className="btn-frame">
                                    <span className="procent">{ conv.percentage }% </span>
                                    <ul className="rating">
                                        {
                                            Array.from(new Array(3), (x, i) => {
                                                return i < conv.predictedLabel ?
                                                    <li key={i}><a href="#"><img src="assets/images/img-16.png"
                                                                         alt="img description"/></a></li> :
                                                    <li key={i}><a href="#"><img src="assets/images/img-15.png"
                                                                         alt="img description"/></a></li>;
                                            })
                                        }
                                    </ul>
                                    <span className="gray-text">Low, some explanation?</span>
                                </div>
                            </li>
                            })
                        }
                    </ul>
                </section>
                <div className="cash">
                    <ul className="cash-list">
                        <li><strong className="title">$ { availableCash }</strong><span className="gray-text">Available cash to deposit</span></li>
                        <li><strong className="title">$ { monthlyCash }</strong><span className="gray-text">Monthly available cash</span></li>
                    </ul>
                </div>
            </div>
        );
    }
};