import React, { Component } from 'react';

export default class PhotosCarousel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            isOpen: nextProps.isOpen,
            images: nextProps.data,
            index: nextProps.index
        });
    }

    closeModal(e) {
        e.preventDefault();
        let body = document.getElementsByTagName('body');
        body[0].classList.remove('modal-open');
        this.setState({
            isOpen: false
        });
    }

    nextPhoto(length, e) {
        e.preventDefault();
        let { index } = this.state;
        index = index + 1 === length ? 0 : ++index;
        this.setState({
            index: index
        });
    }

    prevPhoto(length, e) {
        e.preventDefault();
        let { index } = this.state;
        index = index  === 0 ? length - 1 : --index;
        this.setState({
            index: index
        });
    }

    render() {
        const { index } = this.state;
        const { images } = this.state;
        const { isOpen } = this.state;
        if (!isOpen) return null;
        let body = document.getElementsByTagName('body');
        body[0].classList.add('modal-open');
        return (
            <div id="popup2" className="fancybox-wrap" onClcik={this.closeModal.bind(this)}>
                <div className="fancybox-photos fancybox-opened">
                    <div className="fancybox-skin">
                        <div className="fancybox-inner">
                            <section className="lightbox">
                                <h2>
                                    <a href="#" onClick={this.closeModal.bind(this)}><img src="assets/images/img-01.png" /></a>
                                </h2>
                                <div className="photo-info">
                                    <span className="photo-source">{index + 1} from {images.length}</span>
                                    <span className="photo-source">Sourse: <a hres={images.link}>{images.source}</a></span>
                                </div>
                                <div className="photo-carousel">
                                    <div className="photo-current">
                                        <img src={images[index].link}/>
                                    </div>
                                    <a href="#" className="prev-photo" onClick={this.prevPhoto.bind(this, images.length)}><span></span></a>
                                    <a href="#" className="next-photo" onClick={this.nextPhoto.bind(this, images.length)}><span></span></a>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}