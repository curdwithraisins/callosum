import React, { Component } from 'react';
import PhotosCarousel from './PhotosCarousel';

export default class Photos extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenList: false,
            isOpenPhoto: false,
            numberOfOpen: 20
        };
    }

    /**for test*/
/*    componentWillMount() {
        for (let j = 0; j < 100; j++) {
            this.props.photos.push(this.props.photos[0]);
        }
    }*/

    handlerOnClick(e) {
        e.preventDefault();
        this.setState({
            isOpenPhoto: false,
            numberOfOpen: this.state.numberOfOpen + 20
        });
    }

    someMethod() {
        if (this.props.photos.length > 7) {
            return <div className="btn-box"><a className="load-more" href="#"
                                               onClick={this.handlerOnClick.bind(this)}>See { this.props.photos.length - this.state.numberOfOpen } more</a></div>
        }
    }

    openImage(i, e) {
        e.preventDefault();
        this.setState({
            isOpenList: false,
            isOpenPhoto: true,
            index: i
        });
    }

    render() {
        const {photos, active} = this.props;
        if (!photos) return null;
        const {isOpenList, isOpenPhoto, index, numberOfOpen} = this.state;
        return (
            <div className={active == 2 ? 'col-left active-block' : 'col-left'}>
                <section className="photos">
                    <h1>Photos
                    <span className="counter">
                        <a href="#" onClick={this.handlerOnClick.bind(this)}>{photos.length}</a>
                    </span>
                    </h1>
                    <ul className="photo-list">
                        {
                            photos.map((photo, i) => {
                                if (i >= numberOfOpen) return null;
                                return (
                                    <li key={i}>
                                        <img src={photo.link} onClick={this.openImage.bind(this, i)}/>
                                    </li>
                                )
                            })
                        }
                    </ul>
                    { this.someMethod() }
                    <PhotosList isOpen={isOpenList} data={photos} openImage={this.openImage.bind(this)}/>
                    <PhotosCarousel isOpen={isOpenPhoto} data={photos} index={index}/>
                </section>
            </div>
        );
    }
}