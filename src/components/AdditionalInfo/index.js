import React, { Component } from 'react';
import AboutBlock from './About';
import Photos from './Photos';
import PostAndTweets from './PostAndTweets';

export default class AdditionalInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 1
        }
    }

    tabChange(newTab) {
        this.setState({active: newTab});
    }

    render() {
        const { additionalInfo } = this.props;
        const { active } = this.state;
        if (!additionalInfo) return null;
        return (
            <div className="info-block">
                <ul className="info-menu">
                    <li className={active == 1 && 'active'} onClick={this.tabChange.bind(this, 1)}>About</li>
                    <li className={active == 2 && 'active'} onClick={this.tabChange.bind(this, 2)}>Photos</li>
                    <li className={active == 3 && 'active'} onClick={this.tabChange.bind(this, 3)}>Posts and Tweets</li>
                </ul>
                <AboutBlock active={active} about={additionalInfo.about} />
                <Photos active={active} photos={additionalInfo.photos} />
                <PostAndTweets active={active} posts={additionalInfo.posts} />
            </div>
        )
    }
};