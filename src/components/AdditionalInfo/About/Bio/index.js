import React, { Component } from 'react';

export default class Bio extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 2,
            previewShow: 2
        };
    }

    bindClick(e) {
        e.preventDefault();
        const {data} = this.props;
        this.setState({
            number: this.state.number === this.state.previewShow ? data.length : this.state.previewShow
        });
    }

    someMethod(data) {
        if (data.length > this.state.number) {
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>See {data.length - this.state.number} more</a>
        } else if (data.length === this.state.number && data.length > this.state.previewShow)
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>Hide</a>
    }

    render() {
        const {data} = this.props;
        if (!data) return null;
        return (
            <section className="about">
                <h1>About</h1>
                <div>
                    {data.map((about, i) => {
                        if (i >= this.state.number) return null;
                        return (
                            <div className="text-holder" key={i}>
                                <blockquote>
                                    <q>{ about.status }</q>
                                </blockquote>
                                <div className="row">
                                    <div className="img-social"><a href={ about.link } target="_blank"><img src={ 'assets/images/' + about.source + '.png' } alt={ about.source }/></a>
                                    </div>
                                    <span className="gray-text">{ about.source }</span>
                                </div>
                            </div>
                        );
                    })}
                    { this.someMethod(data) }
                </div>
            </section>
        );
    }
}