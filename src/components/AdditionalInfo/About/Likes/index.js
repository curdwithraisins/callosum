import React, { Component } from 'react';

export default class Likes extends Component {
    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <section className="likes">
                <h1>Likes</h1>
                <ul className="likes-list">
                    {
                        data.map((like, i) => {
                            return (
                                <li key={i}>
                                    <div className="img-wrapper">
                                        <a href={ like.link } ><img src={ like.linkPicture || "assets/images/img-11.png"} alt="img description"/></a>
                                    </div>
                                    <p>{ like.name }</p>
                                    <span className="gray-text">{ like.group }</span>
                                </li>
                            )
                        })
                    }
                </ul>
                <div className="btn-box">
                    <a href={ data.link }>See all likes on Facebook</a>
                </div>
            </section>
        )
    }
}