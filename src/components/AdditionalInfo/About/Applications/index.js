import React, { Component } from 'react';

export default class Applications extends Component {
    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <section className="applications">
                <h1>Applications</h1>
                <ul className="applications-list">
                    {
                        data.map((app, i) => {
                            return <li key={i}>
                                <a href={app.link}><div className="img-wrapper">
                                    <img src={app.image || "assets/images/img-11.png"} />
                                </div></a>
                                <p>{app.title}</p>
                            </li>
                        })
                    }
                </ul>
            </section>
        )
    }
}