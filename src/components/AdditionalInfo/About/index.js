import React, { Component } from 'react';
import InfoList from './InfoList';
import Bio from './Bio';
import ContactInfo from './ContactInfo';
import SocialNetworks from './SocialNetworks';
import Likes from './Likes';
import Applications from './Applications';
import Interests from './Interests';

export default class AboutBlock extends Component {
    render() {
        const { about, active } = this.props;
        if (!about) return null
        return (
            <div className={active == 1 ? 'col-left active-block' : 'col-left'}>
                <InfoList name={'Education'} data={about.education} />
                <InfoList name={'Employment'} data={about.employment} />
                <Bio data={about.bio} />
                <Interests  data={about.interests} />
                <ContactInfo data={about.contactInfo} />
                <SocialNetworks data={about.socialNetworks} />
                <Likes data={about.likes} />
                <Applications data={about.applications} />
            </div>
        )
    }
};