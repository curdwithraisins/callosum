import React, { Component } from 'react';

export default class SocialNetworks extends Component {
    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <section className="social-section">
                <h1>Social Networks</h1>
                <ul className="social-networks">
                    {data.map((network, i) => {
                        return (
                            <li key={i}>
                                <div className="img-box"><img src={ network.linkPicture || "assets/images/img-12.png"} /></div>
                                <a href={ network.link } target="_blank"><img src={ 'assets/images/' + network.type + '.png' } /></a>
                            </li>
                        );
                    })}
                </ul>
            </section>
        );
    }
}