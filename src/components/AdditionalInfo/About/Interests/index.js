import React, { Component } from 'react';

export default class Interests extends Component {
    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <section className="employment-section">
                <h1>Interests</h1>
                <div className="text-holder">
                    {
                        data.map((interest, i) => {
                            return <span key={i} className="interest">{ interest }</span>
                        })
                    }
                </div>
            </section>
        )
    }
}