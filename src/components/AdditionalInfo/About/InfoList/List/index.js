import React, { Component } from 'react';

export default class List extends Component {
    render () {
        const { data } = this.props;
        if (!data) return null;
        return (
            <div className="row">
                <strong className="title">{data.company}</strong>
                <p> { data.position && data.position }
                    { (data.position && data.place) && ', '}
                    { data.place && data.place }
                    { data.period && <span className="dot gray-text">{ data.period }</span> }</p>
            </div>
        )
    }
}