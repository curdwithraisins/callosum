import React, { Component } from 'react';
import List from './List/';

export default class InfoList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 1,
            previewShow: 1
        };
    }

    bindClick(e) {
        e.preventDefault();
        const { data } = this.props;
        this.setState({
            number: this.state.number === this.state.previewShow ? data.length : this.state.previewShow
        });
    }

    someMethod (data) {
        if (data.length > this.state.number) {
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>See {data.length - this.state.number} more</a>
        } else if (data.length === this.state.number && data.length > this.state.previewShow)
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>Hide</a>
    }

    render() {
        const { data } = this.props;
        if (!data) return null;
        const { name } = this.props;
        return (
            <section className="employment-section">
                <h1>{ name }</h1>
                <div className="text-holder">
                    <div className="load-holder">
                        { data.map((info, i) => {
                            if (i >= this.state.number) return null;
                            return <List key={i} data={info}/>; })
                        }
                    </div>
                    { this.someMethod(data) }
                </div>
            </section>
        );
    }
}