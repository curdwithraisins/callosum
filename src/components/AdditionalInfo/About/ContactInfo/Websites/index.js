import React, { Component } from 'react';

export default class Websites extends Component {
    constructor(props) {
        super(props);
        this.state = {
            number: 2,
            previewShow: 2
        };
    }

    bindClick(e) {
        e.preventDefault();
        const {data} = this.props;
        this.setState({
            number: this.state.number === this.state.previewShow ? data.length : this.state.previewShow
        });
    }

    someMethod(data) {
        if (data.length > this.state.number) {
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>See {data.length - this.state.number} more</a>
        } else if (data.length === this.state.number && data.length > this.state.previewShow)
            return <a className="load-more" href="#"
                      onClick={this.bindClick.bind(this)}>Hide</a>
    }

    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <li>
                <span className="gray-text">Websites</span>
                <ul className="website-list">
                    {data.map((website, i) => {
                        if (i >= this.state.number) return null;
                        return <li key={i}><a href={ website } target="_blank">{ website }</a></li>;
                    })}
                    <li>{ this.someMethod(data) }</li>
                </ul>
            </li>
        );
    }
}