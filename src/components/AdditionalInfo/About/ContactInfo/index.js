import React, { Component } from 'react';
import Websites from './Websites/';

export default class ContactInfo extends Component {
    render() {
        const { data } = this.props;
        if (!data) return null;
        return (
            <section className="contact">
                <h1>Contact info</h1>
                <div className="text-holder">
                    <ul className="contact-list">
                        {
                            data.phones.map((phone, i) => {
                                return <li key={i} className={ phone.fromCsv && "csv" }><a href={ 'tel:' + phone.number } >{ phone.number }</a>
                                        { phone.fromCsv && <span className="gray-text">from CSV</span> }
                                </li>
                            })
                        }
                        { !!data.website.length && <Websites data={data.website} /> }
                    </ul>
                    <ul className="contact-list">
                        {
                            data.email.map((email, i) => {
                                return <li key={i} className={email.fromCsv && "csv"}><a href={'mailto:' + email.email} >{ email.email }</a>
                                    { email.fromCsv && <span className="gray-text">from CSV</span> }
                                </li>
                            })
                        }
                        {
                            data.other.map((other, i) => {
                                return <li key={i}><a href="">other.value</a></li>
                            })
                        }
                    </ul>
                </div>
            </section>
        );
    }
}