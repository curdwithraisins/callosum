import React, { Component } from 'react';

export default class PostAndTweets extends Component {
    render() {
        const { posts, active } = this.props;
        if (!posts) return null;
        return (
            <div className="col-left" className={active == 3 ? 'col-left active-block' : 'col-left'}>
                {
                    posts.map((post) => {
                        return <div className="post-block">
                            <span className="post-title">{post.string}</span>
                            <ul className="post-list">
                                {
                                    post.posts.map((mes, i) => {
                                        if (i >= 5) return null;
                                        return <li key={i}>
                                            
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    })
                }
            </div>
        )
    }
};