import React, { Component } from 'react';

export default class Error extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: props.isOpen,
            err: props.err
        }
    }

/*    componentWillReceiveProps(nextProps) {
        this.setState({
            isOpen: nextProps.isOpen,
            err: nextProps.err
        });
    }*/

    closeModal(e) {
        e.preventDefault();
        let body = document.getElementsByTagName('body');
        body[0].classList.remove('modal-open');
        this.setState({
            isOpen: false
        });
    }

    render() {
        const { isOpen } = this.state;
        if (!isOpen) return null;
        const { err } = this.state;
        let body = document.getElementsByTagName('body');
        body[0].classList.add('modal-open');
        return (
            <div id="popup2" className="fancybox-wrap" onClick={this.closeModal.bind(this)}>
                <div className="fancybox-photos fancybox-opened">
                    <div className="fancybox-skin">
                        <div className="fancybox-inner">
                            <section className="lightbox">
                                <h2><a href="#" onClick={this.closeModal.bind(this)}><img src="assets/images/img-01.png" /></a></h2>
                                <h1>Error</h1>
                                <div className="photo-carousel">{err.message}</div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}