import React, { Component } from 'react';

export default class Map extends Component {
    constructor(props){
        super(props);
        this.state = {
            location: null
        };
    }

    componentWillMount() {
        this.getLocation(this.props.address).then(
            res => this.setState({location: res})
        );
    }

    getLocation(address) {
        return new Promise((resolve, reject) => {
            let geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address}, function (results, status) {
                status == 'OK' ? resolve(results[0].geometry.location) : reject(null)
            })
        })
    }

    setLocation() {
        const mapOptions = {
            zoom: 11,
            center: this.state.location
        };
        let map = new google.maps.Map(document.getElementById("map"), mapOptions);
        let marker = new google.maps.Marker({
            map: map,
            position: this.state.location
        });
    }
    
    render() {
        const { address } = this.props;
        if (!address) return null;
        const { location } = this.state;
        if (!location) return null;
        return ( this.setLocation() )
    }
}