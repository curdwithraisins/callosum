import React, { Component } from 'react';
import Map from './Map';

export default class IdentityCard extends Component {
    render() {
        let { basicInfo } = this.props;
        if (!basicInfo) return null;
        const livesIn = basicInfo.places.length > 1 ? basicInfo.places[1] : null;
        return (
            <div className="inform-section">
                <div className="inform-holder">
                    { basicInfo.profilePhoto && <div className="foto-holder"><img src={ basicInfo.profilePhoto } alt="img description"/></div> }
                    <div className="description-text">
                        <span className="title strong">
                            <span className="name">{ basicInfo.name } { basicInfo.lastName } </span>
                            { basicInfo.gender && <span className="dot">{ basicInfo.gender }</span> }
                            { basicInfo.age && <span className="dot">{ basicInfo.age.years }
                                { basicInfo.age.source && <span className="gray-text"> { basicInfo.age.source } </span> } </span>
                            }
                            { basicInfo.creditCard && <span className="dot"><img src="assets/images/img-02.png"/></span> }
                            { basicInfo.appleDevice && <span className="dot"><img src="assets/images/img-03.png"/></span> }
                            { basicInfo.androidDevice && <span className="dot"><img src="assets/images/img-04.png"/></span> }
                        </span>
                        { (basicInfo.additionalName && !!basicInfo.additionalName.length) && <span className="proffesion">{ basicInfo.additionalName.join(', ') }</span> }
                        { basicInfo.profession && <span className="proffesion strong">{ basicInfo.profession }</span> }
                        <ul className="description-list">
                            { (basicInfo.places && !!basicInfo.places.length) && <li><p><span className="gray-text">From</span> { basicInfo.places[0] }</p></li> }
                            { (basicInfo.usesLanguage || !!basicInfo.languages.length) && <li>
                                <p><span className="gray-text">Knows </span>
                                    { basicInfo.usesLanguage && basicInfo.usesLanguage + "(uses)" }
                                    { (basicInfo.usesLanguage && !!basicInfo.languages.length) && ', ' }
                                    { !!basicInfo.languages.length && basicInfo.languages.join(', ') }
                                </p></li>
                            }
                            { basicInfo.places.length > 1 && <li><p><span className="gray-text">Lives in</span> { basicInfo.places[1] }</p></li> }
                        </ul>
                        <ul className="description-list">
                            { basicInfo.birthday && <li><p><span className="gray-text">Born</span><time dateTime={ basicInfo.birthday }> { basicInfo.birthday }</time></p></li> }
                            { basicInfo.familyRelations && basicInfo.familyRelations.map((relation, i) => { return <li key={i}><p>{relation.type} <a href={relation.link}>{ relation.name}</a></p></li>;})}
                        </ul>
                        { basicInfo.campaign && <span className="gray-text data">{ basicInfo.campaign }</span> }
                    </div>
                </div>
                <div className="map-holder" id="map"> { !!livesIn && <Map address={livesIn} /> }</div>
            </div>
        );
    }
}