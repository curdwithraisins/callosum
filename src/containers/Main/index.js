import React, { Component } from 'react';
import { connect } from 'react-redux';

import IdentityCard from '../../components/IdentityCard/';
import Conversion  from '../../components/Conversion';
import AdditionalInfo from '../../components/AdditionalInfo';
import Error from '../../components/Error';

import { getLeadInfo } from '../../actions/get.actions';
import { ErrorRequestNumber } from '../../actions/error.actions';

let count = 0;

export default class Main extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenError: false
        }
    }

    componentWillMount () {
        this.props.getLeadInfo(this.props.params.leadId);
    }



    shouldComponentUpdate(nextProps) {
        if (nextProps.err) {
            //this.setState({isOpenError: true});
            return true;
        }
        if (nextProps.params.leadId !== this.props.params.leadId)
            this.props.getLeadInfo(this.props.params.leadId);
        if ((nextProps.lead || nextProps.lead.data.v2.besicInfo) && count < 2)
            setTimeout(() => {
                count++;
                this.props.getLeadInfo(this.props.params.leadId);
            }, 5000);
        else this.props.ErrorRequestNumber();
        return nextProps.lead !== this.props.lead;
    }

    render() {
        if ((!this.props.lead || !this.props.lead.data.v2.basicInfo) && !this.props.err) return <div className="preloader"></div>;
        let { v2 } = this.props.lead.data;
        const { err } = this.props;
        const isOpenError = !!this.props.err;
        if (isOpenError) {
            v2 = {
                basicInfo: {
                    profilePhoto: 'assets/images/foto-01.png',
                    name: 'Anna',
                    lastName: 'Oded',
                    gender: 'f',
                    age: {
                        years: 21
                    },
                    creditCard: true,
                    appleDevice: false,
                    androidDevice: false,
                    additionalName: [
                        'Kate', 'Oleg Petrov'
                    ],
                    profession: 'IT Security Consultant',
                    places: [
                        'Kiev', 'London'
                    ],
                    usesLanguage: 'Rus',
                    languages: [
                        'Eng'
                    ],
                    birthday: '21 August 1983'
                },
                conversion: [
                    {
                        type: 'Probability of conversion',
                        percentage: 13,
                        predictedLabel: 1
                    },
                    {
                        type: 'Verification',
                        percentage: 53,
                        predictedLabel: 2
                    },
                    {
                        type: 'Enrichment',
                        percentage: 83,
                        predictedLabel: 3
                    }
                ],
                availableCash: 1223,
                monthlyCash: 112,
                additionalInfo: {
                    about: {
                        education: [
                            {
                                position: 'R&D Consultant',
                                period: '2001 - 2010'
                            },
                            {
                                position: 'R&D Consultant',
                                period: '2001 - 2010'
                            },
                            {
                                position: 'R&D Consultant',
                                period: '2001 - 2010'
                            },
                            {
                                position: 'R&D Consultant',
                                period: '2001 - 2010'
                            }
                        ]
                    }
                }
            }
            };
        return (
            <div id="main" role="main">
                <div className="container">
                    <IdentityCard basicInfo={v2.basicInfo} map={v2.map} />
                    <Conversion conversion={v2.conversion}  availableCash={v2.availableCash} monthlyCash={v2.monthlyCash} />
                    <AdditionalInfo additionalInfo={v2.additionalInfo} />
                </div>
                <Error isOpen={isOpenError} err={err} />
            </div>
        );
    }
}

const mapStateTopProps = (state) => ({
    lead: state.getReducer.lead,
    err: state.getReducer.err || state.errorReducer.err
});

export default connect(mapStateTopProps, { getLeadInfo, ErrorRequestNumber })(Main);