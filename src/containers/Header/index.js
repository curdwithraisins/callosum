import React, {Component} from "react";
import Error from '../../components/Error';
import { connect } from 'react-redux';
import { postLeadInfo } from '../../actions/post.actions';

export class Header extends Component {
    postRequst(e) {
        e.preventDefault();
        let params = {
            "lead": {
                "name": this.refs.name.value,
                "lastName": this.refs.lastName.value,
                "email": this.refs.email.value,
                "phone": this.refs.phone.value,
                "country": this.refs.country.value
            }
        };
        this.props.postLeadInfo(params);
    }

    render() {
        let isOpenError = false;
        const { err } = this.props;
        if (err) isOpenError = true;
        return (
            <header id="header">
                <div className="container">
                    <div className="logo">
                        <a href="#">
                            <img src="assets/images/logo.jpg" alt="CalloSum Make the right call"/>
                        </a>
                    </div>
                    <div className="inputs">
                        <input ref="name" placeholder="Name"/>
                        <input ref="lastName" placeholder="LastName"/>
                        <input ref="email" placeholder="Email"/>
                        <input ref="phone" placeholder="Phone"/>
                        <input ref="country" placeholder="Country"/>
                        <button className="load-lead-buton" onClick={this.postRequst.bind(this)}>Load lead</button>
                    </div>
                    <div className="header-text">
                        <p>Sandra Martinez</p>
                        <a href="#">Log out</a>
                    </div>
                </div>
                <Error isOpen={isOpenError} err={err} />
            </header>
        );
    }
};

const mapStateToProps = (state) => ({
    lead: state.postReducer.lead,
    err: state.postReducer.err
});

export default connect(mapStateToProps, { postLeadInfo })(Header);