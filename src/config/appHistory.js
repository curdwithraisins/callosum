import { browserHistory, useRouterHistory } from 'react-router';
import { createHistory, createHashHistory} from 'history';

const appHistory = useRouterHistory(createHashHistory)({ queryKey: false });

//const appHistory = useRouterHistory(createHistory)({ basename: '/v2', queryKey: false });

export default appHistory;
