module.exports = {
    apiPrefix : 'http://api.synapseintel.com',
    key       : 'auth',
    auth      : {
        "x-wsse"    : 'W8SUIQElJDp8cLBdHazxUQex9JGws5DWHXv7MGBL',
        "version"   : 'v2',
        "namespace" : 'qa'
    }
};