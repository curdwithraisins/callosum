import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from '../reducers';

const loggerMiddleware = createLogger();

const createStoreWithMiddleware = applyMiddleware(
    thunkMiddleware,
    loggerMiddleware
);

export default function() {
    return createStore(
        rootReducer,
        compose(
            createStoreWithMiddleware,
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    );
}
