import LeadApi from '../api/LeadApi';
import actionsType from './types/get.types';

export function getLeadInfo(leadId) {
    return (dispatch) => {
        return new LeadApi().getLead(leadId).then(
            res => dispatch(receiveResults(res)),
            err => dispatch(receiveError(err))
        )
    }

    function receiveResults(data) {
        return {
            type: actionsType.GET_SUCCESS,
            data
        }
    }

    function receiveError(err) {
        return {
            type: actionsType.GET_ERROR,
            err
        }
    }
}