import actionsType from './types/error.types';

export function ErrorRequestNumber() {
    return (dispatch) =>
        dispatch({
            type: actionsType.ERROR_REQUEST_NUMBER,
            err : {
                err: {
                    message: "Don't receive data. Exceeded number of requests."
                }
            }
        });
}