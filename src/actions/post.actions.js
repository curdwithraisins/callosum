import LeadApi from '../api/LeadApi';
import actionsType from './types/post.types';
import appHistory from '../config/appHistory';

export function postLeadInfo(params) {
    return (dispatch) => {
        return new LeadApi().postLead(params).then(
            res => dispatch(receiveUrls(res)),
            err => dispatch(receiveError(err))
        )
    }
    
    function receiveUrls(res) {
        let location = res.leadUrl.split('#');
        appHistory.push(location[1]);
        //window.location.reload();

        // todo: add on prod
        //window.location.replace(res.leadUrl);
        return {
            type: actionsType.POST_SUCCESS,
            res
        }
    }

    function receiveError(err) {
        return {
            type: actionsType.POST_ERROR,
            err
        }
    }
}