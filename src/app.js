import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import { Router } from 'react-router';

import Store from './store/';

import routes from './routes/';
import appHistory from './config/appHistory';

const store = Store();

ReactDOM.render(
    <Provider store={store}>
        <Router history={appHistory} routes={routes} />
    </Provider>,
    document.getElementById('root')
);