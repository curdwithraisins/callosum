import Requests from './Requests';

export default class LeadApi {
    constructor() {
        this.request = new Requests();
    }

    postLead(params){
        return this.request.post(`/lead/`, params);
    }

    getLead(leadID){
        return this.request.get(`/lead/id/${leadID}`);
    }
}