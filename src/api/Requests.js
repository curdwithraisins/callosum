import request from 'superagent';
import { apiPrefix, auth } from '../config/serverConfig';

export default class Requests {
    constructor() {
        this.apiPrefix = apiPrefix;
        this.auth = auth;
        const hrefParsed = window.location.href.replace(new RegExp('/', 'g'), '.').split('.');
        if (hrefParsed[2] !== "localhost:8000") {
            this.auth.namespace = hrefParsed[2];
            this.auth.version = hrefParsed[5];
        }
    }

    get(url) {
        return this.request({
            url: url,
            method: 'GET'
        });
    }

    post(url, params) {
        return this.request({
            url: url,
            method: 'POST',
            params: params
        });
    }

    request({url, method, params = {}}) {
        return new Promise((resolve, reject) => {
            request(method, `${this.apiPrefix}${url}`)
                .set(this.auth)
                .send(params)
                .end((err, res) =>
                    err ? reject(err) : resolve(res.body.data));
        });
    }
}