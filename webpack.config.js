var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'sourcemap',
  entry: './src/app.js',
  devServer: {
    host: '0.0.0.0',
    port: 8000
  },
  output: {
    path: __dirname + '/public/',
    filename: 'bundle.js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: 'src/index.html',
      inject: false
    })
  ],
  module: {
    loaders: [
      {
        test: /.js?$/,
        loader: 'babel',
        exclude: [/node_modules/, /public/]
      },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }
    ]
  },
  eslint: {
    configFile: '.eslintrc'
  }
};
