Run "npm install" for a start.

If you want to run a project:
1) run "npm run start";
2) open localhost:8080 or another path that will be show in terminal;
3) reload the page for view an another profile.

If you want to build a project:
1) rename "webpack1.js" (for example to "webpack1.js");
2) run "npm run build";
3) open index.html in folder dist;
4) reload the page for view an another profile.