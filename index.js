var express = require('express');
var webpack = require('webpack');
var webpackConfig = require('./webpack.config.js');
var compiler = webpack(webpackConfig);
var app = express();
var path = require('path');

var SERVER_PORT = process.env.PORT || 8000;

app.use(express.static('public'));

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('*', function (req, res) {
  res.sendFile(path.resolve(__dirname, 'public', 'index.html'));
});

app.listen(SERVER_PORT, (err) => {
  if (err) console.log(err);
  console.log(`Server is running on http://localhost:${SERVER_PORT}`);
});
